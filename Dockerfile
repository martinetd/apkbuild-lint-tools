FROM registry.alpinelinux.org/alpine/infra/docker/golang:latest AS gobuilder

RUN <<EOF
set -e
cd /home/build
wget -O- https://gitlab.alpinelinux.org/alpine/infra/atools-go/-/archive/master/atools-go-master.tar.gz \
    | tar xz
cd atools-go-master
./configure --strip
redo
EOF

FROM alpine:edge

RUN apk add --no-cache abuild doas atools spdx-licenses-list git \
    && adduser -D lint

COPY --from=gobuilder --chmod=0755 /home/build/atools-go-master/apkbuild-lint /usr/local/bin/apkbuild-lint

COPY overlay/ /

RUN chmod 0600 /etc/doas.d/buildozer.conf

COPY --from=koalaman/shellcheck /bin/shellcheck /bin/shellcheck

USER lint

CMD changed-aports master | lint
